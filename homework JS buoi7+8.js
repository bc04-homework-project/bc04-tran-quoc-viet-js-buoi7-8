var numArr = [];

function addNum() {
  var num1 = document.getElementById("num").value * 1;
  numArr.push(num1);
  // console.log("numArr: ", numArr);
  document.getElementById("result").innerHTML = numArr;
}

function takePositiveArr() {
  var positiveNum = numArr.filter(function (number) {
    return number > 0;
  });
  // console.log("positiveNum: ", positiveNum);
  // console.log("positiveNum.length: ", positiveNum.length);
  return positiveNum;
}

// yêu cầu 1 - tính tổng số dương
function sumPositiv() {
  // console.log("bai 1");
  if (takePositiveArr().length == 0) {
    document.getElementById("result2").innerHTML = "Không có số dương";
  } else {
    var sumPositiv = takePositiveArr().reduce(function (total, current) {
      return total + current;
    });
    document.getElementById("result2").innerHTML = sumPositiv;
  }
}

// yêu cầu 2 - đếm số dương
function countPositiv() {
  // console.log("bai 2");
  if (takePositiveArr().length == 0) {
    document.getElementById("result3").innerHTML = "Không có số dương";
  } else {
    var countPositiv = takePositiveArr().length;
    document.getElementById("result3").innerHTML = countPositiv;
  }
}

// yêu cầu 3 - tìm số nhỏ nhất
document.getElementById("bai3").onclick = function () {
  // console.log("Bai 3");
  var numMin = numArr[0];
  for (var index = 1; index < numArr.length; index++) {
    currentNum = numArr[index];
    currentNum < numMin ? (numMin = currentNum) : (numMin = numMin);
  }
  numArr.length == 0
    ? (document.getElementById(
        "result4"
      ).innerHTML = `Bạn chưa nhập số người đẹp ơi`)
    : (document.getElementById("result4").innerHTML = numMin);
};

// yêu cầu 4 - tìm số dương nhỏ nhất
document.getElementById("bai4").onclick = function () {
  // console.log("Bai 4");
  if (takePositiveArr().length == 0) {
    document.getElementById("result5").innerHTML = "Không có số dương";
  } else {
    posNumMin = takePositiveArr()[0];
    for (var index = 1; index < numArr.length; index++) {
      currentPosNum = takePositiveArr()[index];
      if (currentPosNum < posNumMin) {
        posNumMin = currentPosNum;
      }
    }
    document.getElementById("result5").innerHTML = posNumMin;
  }
};

// yêu cầu 5 - tìm số chẵn cuối cùng
document.getElementById("bai5").onclick = function () {
  // console.log("Bai 5");
  var evenNum = numArr.filter(function (number) {
    return number % 2 == 0;
  });
  // console.log("evenNum : ", evenNum);
  evenNum.length == 0
    ? (lastEvenNum = -1)
    : (lastEvenNum = evenNum[evenNum.length - 1]);
  document.getElementById("result6").innerHTML = lastEvenNum;
};

// yêu cầu 6 - Đổi chỗ 2 giá trị trong mảng theo vị trí
document.getElementById("bai6").onclick = function () {
  // console.log("Bai 6");
  var index1 = document.getElementById("index1").value * 1;
  var index2 = document.getElementById("index2").value * 1;
  var numToChange = numArr[index1];
  numArr[index1] = numArr[index2];
  numArr[index2] = numToChange;
  document.getElementById("result7").innerHTML = numArr;
};

// yêu cầu 7 - Sắp xếp mảng theo thứ tự tăng dần
document.getElementById("bai7").onclick = function () {
  // console.log("Bai 7");
  numArr.sort(function (firstNum, secondNum) {
    if (secondNum > firstNum) {
      return -1;
    }
  });
  document.getElementById("result8").innerHTML = numArr;
};

// yêu cầu 8 - Tìm số nguyên tố đầu tiên
document.getElementById("bai8").onclick = function () {
  // console.log("Bai 8");
  var currentNum2;
  for (var i = 0; i < numArr.length; i++) {
    currentNum2 = numArr[i];
    if (currentNum2 < 2) {
      continue;
    } else if (currentNum2 == 2 || currentNum2 == 3) {
      document.getElementById("result9").innerHTML = currentNum2;
      break;
    } else {
      for (n = 2; n <= Math.sqrt(currentNum2); n++) {
        if (currentNum2 % n == 0) {
          var primeNum = 0;
          break;
        } else {
          var primeNum = currentNum2;
        }
      }

      if (primeNum > 0) {
        document.getElementById("result9").innerHTML = primeNum;
        break;
      }
    }
  }
};

// yêu cầu 9 - Đếm số nguyên
var numArr2 = [];
function addNum2() {
  var num2 = document.getElementById("num2").value * 1;
  numArr2.push(num2);
  // console.log("numArr2: ", numArr2);
  document.getElementById("result10").innerHTML = numArr2;
}

document.getElementById("bai9").onclick = function () {
  // console.log("Bai 9");
  var integer = numArr2.filter(function (number2) {
    return number2 == 0 || Number.isInteger(number2);
  });
  // console.log("integer: ", integer);
  // console.log("integer.length: ", integer.length);

  if (integer.length === 0) {
    document.getElementById("result11").innerHTML = "Không có số nguyên";
  } else {
    document.getElementById("result11").innerHTML = integer.length;
  }
};

// yêu cầu 10 - so sánh số lượng số âm và số dương
document.getElementById("bai10").onclick = function () {
  // console.log("Bai 10");

  var negativeNum = numArr.filter(function (number3) {
    return number3 < 0;
  });
  // console.log("negativeNum: ", negativeNum);
  // console.log("negativeNum.length: ", negativeNum.length);

  var positiveNum = takePositiveArr();

  if (negativeNum.length == 0 && positiveNum.length == 0) {
    document.getElementById(
      "result12"
    ).innerHTML = `Bạn chưa nhập số người đẹp ơi`;
  } else if (negativeNum.length == positiveNum.length) {
    document.getElementById("result12").innerHTML = `số âm = số dương`;
  } else if (negativeNum.length > positiveNum.length) {
    document.getElementById("result12").innerHTML = `số âm > số dương`;
  } else {
    document.getElementById("result12").innerHTML = `số âm < số dương`;
  }
};
